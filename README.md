# BTI7062-AlDa-Homework-3
## Tasks

1. prove that
    1. bases are asymptotically irrelevant
    2. constant exponents are relevant
2. verify the increasing ordering of these ACs (trick: log-log ;-)
3. what are the ACs of our summation laws on Page 35?

## PDF
The ready to submit PDF file can be found [here](https://gitlab.com/severinkaderli/BTI7062-AlDa-Homework-3/builds/artifacts/master/raw/TEAM-Kaderli-Kilic-Schaer.pdf?job=PDF).