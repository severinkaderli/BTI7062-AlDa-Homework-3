---
title: "Homework Task 3"
author: [Severin Kaderli, Alan Kilic, Marius Schär]
date: "2018-10-04"
titlepage: true
toc-own-page: true
toc-title: "Inhaltsverzeichnis"
header-includes: |
    \usepackage{amsmath}
...

# Task 1.1
Any logarithm can be expressed as two logarithms of a different base, as follows:
$$
log_{b}(n) = \frac{log_{k}(n)}{log_{k}(b)} \;\; \text{given} \;\; b,k > 1
$$

This property will be useful later. To prove that $log_{a}(n) \text{ is } \Theta(log_{b}(n))$:

We need to find a $c$ so that $log_{a}(n) \leq c \cdot log_{b}(n)$.

Because $a$ is constant, we can say $c = \frac{1}{log_{c}(b)}$,

then clearly $log_{a}(n) \leq \frac{1}{log_{c}(a)} \cdot log_{c}(n)$ is true for any $n > 1$ and $c > 1$.

This all means that logarithms can be converted from one base to another using a constant, and because for complexity analysis we ignore $c$onstants, the base of the logarithm doesn't matter.

# Task 1.2
$c_{1} \cdot n^{c_{2}} = \Theta (c_{3} \cdot n^{c_{2}}$

From the definition of $\Theta (g(n))$

$0 \leq c_{1} \cdot n^{c_{2}} \leq c_{3} \cdot n^{c_{4}}$

For $c_{1} \cdot n^{c_{2}} \leq c_{3} \cdot n^{c_{4}}$ and $c_{2} = c_{4}$

Thus, we assume $c_{2} \neq c_{4}$

*Basic definition of O-Notation:* If $f(g)$ would be in $\Theta (g(n))$ there exists a rational number $c$ for which it holds that $f(g) = c \cdot g(n)$, for $n \geq n_{0}$

assume that $c_{1} = 1, c_{2} = 2, c_{3} = 3, c_{4} = 4$, meaning:

$1 \cdot n^{2} = 3 \cdot n^{4}$

Thus, LHS can never be equal to RHS if $c_{2} \neq c_{4}$

Therefore to achive the equality of the terms the exponents have to be the same. $c_{1}$ and $c_{3}$ are treated as constants and do not change the value for large $n$ values.

# Task 2
The increasing ordering of the ACs can be easily verified by plotting them in a graph.

![Order of Asymptotic Complexities](./assets/graph.png)

# Task 3
## Arithmetic Series

$$
\begin{aligned}
\sum_{i=0}^{n}i &= \frac{n(n+1)}{2} \\
&= (n^{2} + n) \cdot \frac{1}{2} \\
&= 0.5n^{2} + 0.5n
\end{aligned}
$$ 

The highest term is $n^{2}$ and therefore the summation law for the arithmetic series is $\Theta(n^{2})$.

## Geometric Series

$$
\begin{aligned}
\sum_{i=0}^{n}x^{i} &= \frac{x^{n+1}-1}{x-1} \\
&= (x^{n+1} - 1) \cdot (\frac{1}{x-1})
\end{aligned}
$$

The highest term is $x^{n}$ and therefore the summation law for the geometric series is $\Theta(c^{n})$.
